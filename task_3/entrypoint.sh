#!/usr/bin/env sh

args="$@"

json-server /db.json --static /opt/html --host 0.0.0.0 $args
