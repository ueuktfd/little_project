#!/usr/bin/env bash

# Set sane defaults for bash
set -o errexit
set -o nounset
set -o pipefail

BASE_DIR="/opt"
LOG_LINES_KEEP=10000
PRUNE_DIRS=()

#######################################
# Get directories to clean up
#
# Globals:
#   PRUNE_DIRS
# Arguments:
#   none
#######################################
function get_dirs() {
  files=$(find . -name .prune-enable)
  if [[ -z "${files}" ]]; then
    echo "No dirs found for clean up."
    exit 0
  fi
  echo "These directories for clean up are found:"
  for file in ${files}; do
    dir=$(dirname "${file}")
    echo "${dir}"
    PRUNE_DIRS+=("${dir}")
  done

  readonly PRUNE_DIRS
}

#######################################
# Remove crash dumps
#
# Globals:
#   PRUNE_DIRS
# Arguments:
#   none
#######################################
function remove_crash_dumps() {
  echo "Removing crash dumps..."
  for dir in "${PRUNE_DIRS[@]}"; do
    rm -v -f "${dir}/crash.dump"
  done
}

#######################################
# Identify the log files larger than 1M and reduce them
#
# Globals:
#   PRUNE_DIRS
# Arguments:
#   none
#######################################
function reduce_big_logs() {
  echo "Reducing big logs..."
  for dir in "${PRUNE_DIRS[@]}"; do
    big_logs=$(find "${dir}" -name "*.log" -size +1M)
    if [[ -n "${big_logs}" ]]; then
      for log in ${big_logs}; do
        reduce_log "${log}"
      done
    fi
  done
}

#######################################
# Check the number of lines in the file and reduce it to the LOG_LINES_KEEP number
#
# Globals:
#   LOG_LINES_KEEP
# Arguments:
#   $1: path to the log which should be shortened
#######################################
function reduce_log() {
  log_name=$1
  lines_in_file=$(wc -l "${log_name}" | cut -f1 -d " ")
  if (( lines_in_file > LOG_LINES_KEEP )); then
    if ! command -v sponge &> /dev/null; then
      tail -n "${LOG_LINES_KEEP}" "${log_name}" > "${log_name}.tmp"
      mv "${log_name}.tmp" "${log_name}"
    else
      tail -n "${LOG_LINES_KEEP}" "${log_name}" | sponge "${log_name}"
    fi
    echo "[done] ${log_name}"
  else
    echo "[skipped] ${log_name} file is too short"
  fi
}

#######################################
# Entry point of the script
#
# Globals:
#   BASE_DIR
# Arguments:
#   none
#######################################
function main() {
  cd "${BASE_DIR}"
  get_dirs
  remove_crash_dumps
  reduce_big_logs
}

main
