const http = require('http');
const programs = require('./src/programs')
const server = http.createServer(function(req, res) {

  programs.get().then((programsList) => {

    res.writeHead(200, {"Content-Type": "text/html"});

    res.write('<!DOCTYPE html>'+
        '<html>'+
        ' <head>'+
        ' <meta charset="utf-8" />'+
        ' <title>Programs</title>'+
        ' </head>'+
        ' <body>'+
        ' <h1>List of programs</h1>'+
        programsList +
        ' </body>'+
        '</html>');
    res.end();
  });
});


server.listen(8080);
