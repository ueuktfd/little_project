const fetch = require("node-fetch");
const Validator = require('jsonschema').Validator;
const v = new Validator();

const PROGRAMS_API_URL="http://localhost:3000/api/v1/programs";

const programsSchema = {
  "type": "array",
  "items": {
    "properties": {
      "id": { "type": "number" },
      "name": { "type": "string" },
      "rating": { "type": "string" },
      "year": { "type": "number" },
      "plus_only": { "type": "boolean" },
    },
    "required": ["name", "rating", "year", "plus_only"]
  }
};

getPrograms = async () => {
  const response = await fetch(PROGRAMS_API_URL);
  if (response.status >= 200 && response.status <= 299) {
    const jsonResponse = await response.json();
    return v.validate(jsonResponse, programsSchema).valid ? handleResponse(jsonResponse) : showError();
  } else {
    return showError();
  }
}

handleResponse = (response) => {
  const totalNumber = response.length;
  const filteredResponse = filterResponse(response);
  return generateTemplate(filteredResponse, totalNumber);
}

filterResponse = (response) => {
  return filteredResponse = response.filter(movie => movie.rating <= 16 && movie.plus_only);
}

generateTemplate = (movies, totalNumbers) => {
  let html = '';
  if (movies.length > 0) {
    movies.map((movie) => {
      html += "<h3>" + movie.name + "</h3>";
      html += "<p>Release year: " + movie.year + "</p>"
    });
  } else {
    html += 'No programs found'
  }
  html += "<hr>";
  html += "<p>Total programs: " + totalNumbers + "</p>";
  html += "<p>Programs met the criteria: " + movies.length + "</p>";
  return html;
}

showError = () => {
  return "<h2>The error occurred</h2><p>Please try again later</p>";
}

exports.get=getPrograms;
